
vcsrepo { "any":
    ensure => latest,
    provider => git,
    source => "git@bitbucket.org:kenny13/octopress.blog.git",
    revision => "master"
}
